package com.example.midterm1

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import kotlinx.android.synthetic.main.home_layout.*

class HomeActivity : AppCompatActivity() {
    private lateinit var adapter:RecyclerViewAdapter
    val activity = MainActivity()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_layout)
        init()
    }

    private fun launch(model : InfoModel) {
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(model.data)
        recyclerView.adapter = adapter
    }

    private fun getInfo() {
        DataLoader.getRequest("users", object: CustomCallback{
            override fun onSuccess(result: String) {
                var model = Gson().fromJson(result, InfoModel::class.java)
                launch(model)
            } })
    }

    private fun init() {
        getInfo()
        sign_out.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, MainActivity :: class.java))
        }
    }


}