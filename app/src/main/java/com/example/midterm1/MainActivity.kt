package com.example.midterm1

import android.os.Bundle
import android.content.DialogInterface
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.util.Patterns
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.password
import kotlinx.android.synthetic.main.activity_main.username

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()
        sign_up.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
            finish()
        }

        log_in.setOnClickListener {
            if (validateMail(username) && validatePass(password)) {
                logIn(username.text.toString(), password.text.toString())
            }
        }

        forgot_password.setOnClickListener {
            changePass()
        }
    }

    fun validateMail(customID : EditText) : Boolean{
        if (customID.text.toString().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(customID.text.toString()).matches()) {
            customID.error = "Enter Valid Email"
            customID.requestFocus()
            return false
        }
        return true
    }

    fun validatePass(customID : EditText) : Boolean{
        if (customID.text.toString().isEmpty() || customID.text.toString().length <= 4) {
            customID.error = "Enter Valid Password (Minimum 5 characters)"
            customID.requestFocus()
            return false
        }
        return true
    }

    private fun resetPass(email : String) {
        Log.d("Tag", "resetPass")
        auth.sendPasswordResetEmail(email)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(this, "Email Sent", Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun changePass() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Forgot Password")
        val view = layoutInflater.inflate(R.layout.forgot_pass_layout, null)
        val resetMail = view.findViewById<EditText>(R.id.f_pass_username)
        builder.setView(view)
        builder.setPositiveButton("Reset", DialogInterface.OnClickListener { _, _ ->
            if (validateMail(resetMail)) {
                resetPass(resetMail.text.toString())
            }
            else {
                changePass()
                Toast.makeText(this, "Input Right Format", Toast.LENGTH_SHORT).show()
            }
        })
        builder.setNegativeButton("Close", DialogInterface.OnClickListener { _, _ ->})
        builder.show()
    }

    private fun updateUI(currentUser : FirebaseUser?) {
        if (currentUser != null) {
            startActivity(Intent(this, HomeActivity :: class.java))
        }
        else {
            Toast.makeText(baseContext, "Log In", Toast.LENGTH_SHORT).show()
        }
    }

    private fun logIn(ml : String, pass : String) {
        auth.signInWithEmailAndPassword(ml, pass)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    updateUI(null)
                }
            }
    }
}