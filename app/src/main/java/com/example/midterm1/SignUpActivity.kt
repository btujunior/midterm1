package com.example.midterm1


import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.signup_layout.*
import android.widget.Toast



class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signup_layout)
        init()
    }

    private fun createUser(mail : String, pass : String) {
        auth.createUserWithEmailAndPassword(mail, pass)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    startActivity(Intent(this, MainActivity :: class.java))
                    finish()
                } else {
                    Toast.makeText(baseContext, "Failed", Toast.LENGTH_SHORT).show()
                }
            }
    }

    private  fun init() {
        auth = FirebaseAuth.getInstance()
        create_account.setOnClickListener {
            val activity = MainActivity()
            if (activity.validateMail(username) && activity.validatePass(password)) {
                createUser(username.text.toString(), password.text.toString())
            } }
    }
}